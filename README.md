# Knapp AMR challenge

## Table of Contents

- [Knapp AMR challenge](#knapp-amr-challenge)
  - [Table of Contents](#table-of-contents)
  - [About <a name = "about"></a>](#about-)
  - [Getting Started <a name = "getting_started"></a>](#getting-started-)
    - [Prerequisites](#prerequisites)
    - [Build docker and run](#build-docker-and-run)
    - [Build and run the project](#build-and-run-the-project)
    - [Testing](#testing)
  - [Description of the solution](#description-of-the-solution)

## About <a name = "about"></a>

This repo hosts the solution for the problem stated in the Knapp AMR challenge.

The solution is coded in C++ 17 using ROS Foxy and Ubuntu 20.04. In order to make it easier to work, a docker container was also
designed.

## Getting Started <a name = "getting_started"></a>

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

In order to run the demo you need to install Docker on your machine. No Nvidia GPU is required.

```Bash
#Remove old dependencies
sudo apt-get remove docker docker-engine docker.io containerd runc

# Install dependencies
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common

# Install docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

### Build docker and run

To build the image, you just need to:

```bash
# Go to docker folder.
cd docker

# Run the build script.
./build

# Run the container.
./run

```

If you need access from multiple consoles, open a new console and run the run script again.
The container was tested on the following hosts:

- Ubuntu 20.04
- Ubuntu 18.04
- Windows 10 WSL

### Build and run the project

Once you are inside the container you have to build and run the project. Remember that this container has no persistency. This
means that you will have to build everytime you start the container again.

```bash

# Build the project
colcon build

# Source the project
. install/setup.bash

# run the launchfile
ros2 launch knapp_amr demo.launch.py
```

### Testing

Tests are stored in `/test` folder. To run all test simply type:

```bash
#build
colcon build

#test
colcon test

```

In case you need to read the output of the tests run:

```bash
colcon test --event-handlers console_cohesion+

```

## Description of the solution

When node runs, it parses the product file and subscribe to topics.
The node listens for the next order in `/next_order` topic. When a new order arrives, it searches the order in the product 
file. [Yaml-cpp](https://github.com/jbeder/yaml-cpp) is being used for parsing files. It has a very bad performance when it comes
to large yaml files (around 15 seconds to find an order).

If the product was found, it looks for the order files. `OrderFinder.hpp` finds all files inside the folder provided and start
a thread for each file, looking for the order requested. If the order is found, a callback is called.

The callback in `OrderOptimizer.cs` then receives the order. It then calls `getPartsFromOrder()` to retrieve all the parts that are
required for the order. With that vector of parts, a path optimizer is used, so it gets the shortest path to pick-up all parts. The
path finder is a travelsalesman problem implementation taken from [this repo](https://github.com/samlbest/traveling-salesman) and
refactorized to match the challenge requirements. To get the shortest path from the location of the robot, a part called 'robot' is being added. The path finder will recognize it as an element of the list and it will add it to the path. After calling `path_finder_->findPath(parts)` a vector returns with the shortest path possible. Finally All values are printed and also marker array information is published.
